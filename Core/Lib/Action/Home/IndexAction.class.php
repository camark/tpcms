<?php
/**
 * 前台入口模块
 * 
 */
class IndexAction extends HomeAction {
    public function index(){
		$this->display();
	}

	public function dizhibu()
	{

    $cs=D('contract');
    $zimus=$cs->query('select distinct left(tp_contract.mail,1) as zimu from tp_contract order by left(tp_contract.mail,1)');
     
    $this->assign('zimus',$zimus);

    $some_array=array();
    foreach($zimus as $zimu)
    {
      //print $zimu['zimu'];
      $query_str="select c.id,c.name from tp_contract as c where left(c.mail,1)='".$zimu['zimu']."'";
      //print $query_str;
      $mans=$cs->query($query_str); 
      $some_array[$zimu['zimu']]=$mans;
    }

    $this->assign('someone',$some_array);
    //var_dump(($some_array));

		$names=$this->QueryFieldData('name');

		$this->assign('names',$names);

		$this->display();
	}

  public function findman()
  {
    $id=$this->_param('id');
    //print $id;

    if(!$id) $this->error("User id error!");

    $cs=D('contract');

    $man=$cs->where('id='.$id)->limit(1)->select();
    $this->assign('man',$man[0]);
    $this->display();
  }


  public function findmanbyname()
  {
    $name=$this->_post('username');
    //print $name;

    if(!$name) $this->error("User name not set!");

    $cs=D('contract');

    $man=$cs->where("name='".$name."'")->limit(1)->select();

    if(count($man)==0)
      $this->error("User not found!");
    else
    {
      $this->assign('man',$man[0]);
      $this->display('findman');
    }
  }

	private function QueryFieldData($fieldname)
    {
      $modify = D("contract");
      
        
      $data=$modify->distinct(true)->getField($fieldname,true);
      
      return $this->Combine($data);  
    }
	    
    private function Combine($data)
    {
      $result="";
      foreach($data as $d)
      {
        $result=$result.'"'.$d.'",';
      }
      
      return substr($result,0,strlen($result)-1);
    }

}