<?php

class ShengchanAction extends AdminAction
{
	public function add()
	{
		if(!isset($_POST['dosubmit']))
		{
			$this->display();
		}

		else
		{
			
		}
	}

	public function listall()
	{
		$yilaishu=M('yilaishu');

		$query='
				select tp_shenqing.id,tp_shenqing.shenqing_no,tp_shenqing.create_time,tp_user.remark
				from tp_shenqing
				left join tp_user
				on tp_shenqing.create_user_id=tp_user.id
				where tp_shenqing.is_shenhe=0';

	    $vo=$yilaishu->query($query);

	    $this->assign('vos',$vo);
	    $this->display();
	}

	public function shenhe()
	{
		$id = $this->_get('id','intval',0);
		if(!$id)
			$this->error('参数错误!');

		$yilaishu=M('yilaishu');

		$query='
		select tp_yilaishu.id,tp_yilaishu.cunhuo_no,tp_yilaishu.lingjian_name,
		tp_yilaishu.guige,tp_yilaishu.product_no,tp_yilaishu.project_name,
		tp_yilaishu.is_bom,tp_yilaishu.dest,tp_yilaishu.reason,tp_yilaishu.yilaishu_no
		from tp_yilaishu
		left join tp_shenqing
		on tp_shenqing.shenqing_no=tp_yilaishu.shenqing_no
		where tp_shenqing.id='.$id;

		$vos=$yilaishu->query($query);
		$this->assign('vos',$vos);
		$this->assign('shenqing_id',$id);
		$this->display();
	}

	public function queren()
	{
		$shenqing_id=$this->_POST('shenqing_no');
		$note=$this->_POST('shenhe_note');

		if(!$shenqing_id)
			$this->error('参数错误');

		$user_id=session('userid');
		$yilaishu=M('yilaishu');
		$shenqing_no=D('shenqing')->where('id='.$shenqing_id)->getField('shenqing_no');

		$query="update tp_shenqing set is_shenhe=1 where id=".$shenqing_id;
		$query1='update tp_yilaishu set tp_yilaishu.shenhe_state=1,shenhe_user_id='.$user_id.',shenhe_note="'.$note.'" where tp_yilaishu.shenqing_no="'.$shenqing_no.'"';


		if($yilaishu->execute($query) and $yilaishu->query($query1))
		{
			$this->assign("jumpUrl",U('/Admin/Shengchan/listall'));
        	$this->success('审批成功！');		
    	}
    	else
    	{
    		$this->error('审批失败！');
    	}
	}
}